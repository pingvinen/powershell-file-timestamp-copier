# File timestamp copier

Small tool made with powershell scripts, which is able to copy `created at` and `updated at` from a set of files to another copy of that set of files.

I needed this after recovering a large amount of files from a failing disk. The recovery was done through multiple **different** file systems, which meant that the timestamps on the files was lost. It turned out that those timestamps were important to the owner of the files and therefore I made this tool :)
