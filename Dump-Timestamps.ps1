<#
    .SYNOPSIS
        Goes through a target directory and dumps file information as a JSON lines (.jsonl) file
#>

param ([Parameter(Mandatory)][string]$directory)


#
# check that the powershell version is high enough
#
$hostVersion = (Get-Host).Version
$runningVersion = [Version]::new($hostVersion.Major, $hostVersion.Minor, 0)
$requiredVersion = [Version]::new(7, 1, 0) # dictated by the "Join-Path" overload we are using
 
if ($requiredVersion -gt $runningVersion)
{
    Write-Output "You are running PowerShell $runningVersion, but this script requires at least $requiredVersion"
    return
}


#
# setup the output file
#
$outFileLocation = Get-Location
$outFile = Join-Path $outFileLocation "filesystem_timestamps.jsonl"

Write-Host "Output file will be $outFile"



function Write-FileInfo {
    Param ([string]$filePathRelativeToSourceDir, [string]$absoluteFilePath, [System.IO.StreamWriter]$stream)

    $creationTimeUtc = [System.IO.File]::GetCreationTimeUtc($absoluteFilePath).ToString("o")
    $lastWriteTimeUtc = [System.IO.File]::GetLastWriteTimeUtc($absoluteFilePath).ToString("o")
    
    $jsonl = "{""FilePath"": ""$filePathRelativeToSourceDir"", ""CreatedAtUtc"":""$creationTimeUtc"", ""LastUpdatedAtUtc"":""$lastWriteTimeUtc""}"

    $stream.WriteLine($jsonl)
}


#
# let's begin
#
try {
    $stream = [System.IO.StreamWriter]::new($outFile)

    $i = 0

    foreach ($file in Get-ChildItem -Path $directory -Recurse -Name) {
        Write-Host "." -NoNewline
        $absolutePath = Join-Path $directory $file
        Write-FileInfo $file $absolutePath $stream
        $i++
    }

    Write-Host "" # just adding a newline

    Write-Host "Processed $i files"
}
finally {
    $stream.close()
}

Write-Host "All done"
