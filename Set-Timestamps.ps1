<#
    .SYNOPSIS
        Applies timestamps from a JSON lines (.jsonl) file to a target directory
#>

param ([Parameter(Mandatory)][string]$directory, [string]$jsonlFile="")

#
# check that the powershell version is high enough
#
$hostVersion = (Get-Host).Version
$runningVersion = [Version]::new($hostVersion.Major, $hostVersion.Minor, 0)
$requiredVersion = [Version]::new(7, 1, 0) # dictated by the "Join-Path" overload we are using
 
if ($requiredVersion -gt $runningVersion)
{
    Write-Output "You are running PowerShell $runningVersion, but this script requires at least $requiredVersion"
    return
}

if ($jsonlFile -eq "") {
    $here = Get-Location
    $jsonlFile = Join-Path $here "filesystem_timestamps.jsonl"
}


#
# setup the input file
#

function Set-FileInfo {
    Param ([Object]$info, [string]$targetDir)

    #$creationTimeUtc = [System.DateTime]::Parse($info.CreatedAtUtc, "o")
    #$lastWriteTimeUtc = [System.DateTime]::Parse($info.LastUpdatedAtUtc, "o")
    $creationTimeUtc = $info.CreatedAtUtc
    $lastWriteTimeUtc = $info.LastUpdatedAtUtc
    
    $targetFile = Join-Path $targetDir $info.FilePath

    if (Test-Path -Path $targetFile -PathType Leaf) {
        [System.IO.File]::SetCreationTimeUtc($targetFile, $creationTimeUtc)
        [System.IO.File]::SetLastWriteTimeUtc($targetFile, $lastWriteTimeUtc)
        return $true
    } else {
        Write-Host "$targetFile does not exist"
        return $false
    }
}


#
# let's begin
#
$i = 0

$failed = New-Object System.Collections.Generic.List[string]

foreach ($line in Get-Content $jsonlFile) {
    $obj = ConvertFrom-Json $line

    if (Set-FileInfo $obj $directory) {
        Write-Host "." -NoNewline
    } else {
        Write-Host "!" -NoNewline
        $failed.Add($obj.FilePath)
    }

    $i++
}

Write-Host "" # just adding a newline

Write-Host "Processed $i files"

$numFailed = $failed.Count

if ($numFailed -gt 0) {
    Write-Host "$numFailed files failed"
    Write-Host $failed
}

Write-Host "All done"
